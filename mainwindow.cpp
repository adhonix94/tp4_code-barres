
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QActionGroup>
#include <QDir>




MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);

    m_langPath = ":/traductions/langue";
    changerLangue(QString("fr_ca"));
    creerMenuLangue();
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool MainWindow::validaterTexte(const QString& text)
{
    for (const QChar& c : text)
    {
        if (!CONVERSION.contains(c.toUpper()) || c == '*')
        {
            return false;
        }
    }

    return true;
}

void MainWindow::on_pushGenerer_clicked()
{
    // Récupérer le texte entré dans le champ de texte
    QString text = ui->textInput->text();

    qDebug() << text;

    if (!validaterTexte(text))
    {
        qDebug() << "Texte invalide. Veuillez entrer des caractères valides.";
        return;
    }

    // Générer le code-barres
    genererCodeBar(text);

}

void MainWindow::genererCodeBar(const QString& text)
{
    QString texteAvecAsterisks = "*" + text + "*";
    QString codeBarres;

    for (const QChar &c : texteAvecAsterisks) {
        qDebug() << c;
        int index = CONVERSION.indexOf(c.toUpper());
        for (int i = 0; i < 9; ++i) {
            codeBarres.append(code39[index][i] ? '1' : '0');
        }
        // Ajouter un ligne blanche entre les caracteres
        codeBarres.append('0');
    }

    qDebug() << codeBarres;

    dessinerCodeBar(codeBarres);
}

void MainWindow::dessinerCodeBar(const QString &codeBarres)
{
    scene->clear();
    qreal x = 0;
    qreal y = 0;
    qreal petiteBarre = 2;
    qreal epaisseBarre = petiteBarre * 2;
    qreal hauteur = 50;

    QPen pen(Qt::NoPen);
    QBrush bandeNoire(Qt::black);
    QBrush bandeBlanche(Qt::white);

    int longueur = codeBarres.length();
    for (int i = 0; i < longueur; ++i) {
        qreal largeur = (codeBarres[i] == '1') ? epaisseBarre : petiteBarre;
        QBrush brush = (i % 2 == 0) ? bandeNoire : bandeBlanche;
        scene->addRect(x, y, largeur, hauteur, pen, brush);
        x += largeur;
    }

    // Adapte le code-barres à la QGraphicsView et le centre
    QRectF codeBarreRect = scene->itemsBoundingRect();
    qreal horizontal = 100; // Ajustez cette valeur pour augmenter ou réduire l'espace horizontal
    qreal vertical = 50; // Ajustez cette valeur pour augmenter ou réduire l'espace vertical
    QRectF nouveauRect(codeBarreRect.x() - horizontal / 2, codeBarreRect.y() - vertical / 2,
                      codeBarreRect.width() + horizontal, codeBarreRect.height() + vertical);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->fitInView(nouveauRect, Qt::KeepAspectRatio);
    ui->graphicsView->centerOn(codeBarreRect.center());

}

void MainWindow::on_pushEffacer_clicked()
{
    scene->clear();
}

void MainWindow::clearBarcode()
{
    ui->textInput->clear();
    scene->clear();
}


void MainWindow::on_actionQuitter_triggered()
{
    QApplication::quit();
}


void MainWindow::on_actionEffacer_triggered()
{
    clearBarcode();
}

void MainWindow::changerLangue(QString aLangueEtPays) {
    if (m_langCourrant != aLangueEtPays) {
        QTranslator* lTraducteur = new QTranslator(this);
        qApp->removeTranslator(lTraducteur);
        QString lLangue = m_langPath + "/traduction_" + aLangueEtPays + ".qm";
        if(lTraducteur->load(lLangue)) {
            qApp->installTranslator(lTraducteur);
            ui->retranslateUi(this);
            m_langCourrant = aLangueEtPays;
        } else {
            qDebug() << "Fichier de langue non trouvé";
        }
    }
}


void MainWindow::creerMenuLangue()
{

    QActionGroup *lGroupeLangue = new QActionGroup(ui->menuLangue);
    lGroupeLangue->setExclusive(true);

    connect(lGroupeLangue, &QActionGroup::triggered, this,
            &MainWindow::slot_menu_action_triggered);

    // Va chercher le langage par défaut sur cet ordi
    QString localeSysteme = QLocale::system().name();       // e.g. "fr_CA"
    localeSysteme.truncate(localeSysteme.lastIndexOf('_')); // e.g. "fr"

    // m_langPath = QApplication::applicationDirPath(); //Dans un environnement en production, il faudrait utiliser ce path

    QDir dir(m_langPath);
    QStringList listeNomDeFichiers = dir.entryList(QStringList("traduction_*.qm"));

    for (int i = 0; i < listeNomDeFichiers.size(); ++i) {
        // extraction du langage à partir du nom de fichier
        QString localeLangue;
        QString localeLangueEtPays;
        localeLangue = listeNomDeFichiers[i];                         // "traduction_en_us.qm"
        localeLangue.truncate(localeLangue.lastIndexOf('.'));      // "traduction_en_us"
        localeLangueEtPays = localeLangue;
        localeLangueEtPays.remove(0,localeLangueEtPays.indexOf('_') +1); // "en_us"
        localeLangue.truncate(localeLangue.lastIndexOf('_'));      // "traduction_en"
        localeLangue.remove(0, localeLangue.lastIndexOf('_') + 1); // "en"

        QString nomDeLangue = QLocale(localeLangue).nativeLanguageName(); // le nom de la langue dans cette langue
        nomDeLangue = nomDeLangue.left(1).toUpper() + nomDeLangue.mid(1); //première lettre en majuscule

        QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(localeLangue)); //ne sert pas pour l'instant.
        QAction *action = new QAction(ico, nomDeLangue, this);

        action->setCheckable(true);
        action->setData(localeLangueEtPays);

        ui->menuLangue->addAction(action);
        lGroupeLangue->addAction(action);

        // Sélectionne la langue du système par défaut dans le menu
        if (localeSysteme == localeLangue) {
            action->setChecked(true);
        }
    }
}

void MainWindow::slot_menu_action_triggered(QAction *action)
{
    if (0 != action) {
        // Charge le langage selon le data de l'action
        changerLangue(action->data().toString());
    }
}

